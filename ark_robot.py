from base_robot import BaseRobot
from random import choice

class ArkRobot(BaseRobot):
	def __init__(self):
		super(ArkRobot, self).__init__()
		self.current_steps = 0
		self.current_direction = "right" #choice(["right", "left"])
		self.step_counter = 0
		self.passed_parachute = False
		self.current_position = 0

	def _get_opposite_direction(self):
		return "left" if self.current_direction == "right" else "right"

	def _get_delta_by_direction(self):
		return -1 if self.current_direction == "left" else 1

	def next_move(self, parachute=False):
		if parachute and not self.current_position == 0:
			self.passed_parachute = True

		if self.passed_parachute:
			return self.current_direction

		if self.current_steps == self.step_counter:
			self.step_counter += 1
			self.current_steps = 0
			self.current_direction = self._get_opposite_direction()
		else:
			self.current_steps += 1

		self.current_position += self._get_delta_by_direction()

		return self.current_direction
