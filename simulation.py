from typing import NewType
from random import randint, shuffle


class CollisionError(BaseException):
	def __init__(self):
		super(CollisionError, self).__init__()

class OutOfBoundsError(BaseException):
	def __init__(self):
		super(OutOfBoundsError, self).__init__()

class Simulation(object):
	_DIRECTION_TO_DELTA = {
		"left": -1,
		"right": 1,
		"stay": 0
	}
	_EMPTY_CELL = None
	_NUMBER_OF_ROBOTS = 2
	def __init__(self, rail_length, distance_between_robots, *robots):
		super(Simulation, self).__init__()
		if len(robots) != self._NUMBER_OF_ROBOTS:
			raise RuntimeError(f"Number of robots is incorrect: {len(robots)}.")

		self.rail_length = rail_length
		self.distance_between_robots = distance_between_robots
		self.rail = []
		self.robots = list(robots)
		self.current_turn = 0
		self._init_rail()
		self.parachutes = []
		for robot in self.robots:
			self._set_robot_position(robot)

		shuffle(self.robots)

	def _init_rail(self):
		self.rail = [self._EMPTY_CELL] * self.rail_length

	def _set_robot_position(self, robot):
		if len(self.parachutes) < 1:
			middle_position = self.rail_length / 2
			self.parachutes.append(randint(middle_position - self.distance_between_robots * 2, middle_position + self.distance_between_robots * 2))
			self.rail[self.parachutes[0]] = robot
		else:
			self.parachutes.append(self.parachutes[0] + self.distance_between_robots)
			self.rail[self.parachutes[1]] = robot

	def _get_delta_by_direction(self, direction="right"):
		return self._DIRECTION_TO_DELTA[direction] if direction in self._DIRECTION_TO_DELTA.keys() else 0

	def _move_robot(self, robot, direction="right"):
		current_position = self.rail.index(robot)
		next_position = current_position + self._get_delta_by_direction(direction)
		if next_position >= len(self.rail) or next_position < 0:
			raise OutOfBoundsError()
		if self.rail[next_position] is not self._EMPTY_CELL:
			raise CollisionError()

		self.rail[current_position] = self._EMPTY_CELL
		self.rail[next_position] = robot

	def _is_on_parachute(self, robot):
		return self.rail.index(robot) in self.parachutes

	def _print_rail(self):
		positions = self.rail.index(self.robots[0]), self.rail.index(self.robots[1])
		to_print = ""
		for i in range(min(positions)-5, max(positions)+5):
			if self.rail[i] is not None:
				to_print += f"|R"
			elif i in self.parachutes:
				to_print += f"|p"
			else:
				to_print += f"|{i}"				

		print(to_print)

	def _run_turn(self):
		for robot in self.robots:
			direction = robot.next_move(self._is_on_parachute(robot))
			self._move_robot(robot, direction)

		self.current_turn += 1

	def run_simulation(self):
		self.current_turn = 0
		print("Start Positions:")
		self._print_rail()
		while True:
			try:
				self._run_turn()
				print(f"Positions after turn {self.current_turn}")
				self._print_rail()
			except CollisionError:
				print("Collision! Simulation over.")
				return self.current_turn
			except OutOfBoundsError:
				print("OutOfBounds! Stopping simultion.")
				return -1