from simulation import Simulation
from my_robot import MyRobot
from ark_robot import ArkRobot


class Test(object):
	def __init__(self, rail_length, distance_between_robots, expected):
		super(Test, self).__init__()
		self.rail_length = rail_length
		self.distance_between_robots = distance_between_robots
		self.expected = expected

def main(RobotClass, tests):
	for i, test in enumerate(tests):
		simulation = Simulation(test.rail_length, test.distance_between_robots, RobotClass(), RobotClass())
		result = simulation.run_simulation()
		if result > test.expected or result == -1:
			print(f"test {i} failed. Result: {result}, Expected: {test.expected}.")

		else:
			print(f"test {i} passed.")


if __name__ == '__main__':
	main(MyRobot, [
			Test(100, 4, 21),
			Test(100, 5, 37),
			Test(100, 6, 56),
			# Test(1000, 60, 7049),
			# Test(50000, 100, 19749)
		])
